#include "gtest/gtest.h"
#include "fifo.h"
#include "lifo.h"


TEST(fifo,constructorTest){
	lab3::fifo test1;
	EXPECT_EQ(true,test1.is_empty());
	EXPECT_EQ(0,test1.size());
}

TEST(fifo,enqueueTest){
	lab3::fifo test2;
	EXPECT_NO_THROW(test2.enqueue("1"));
}

TEST(fifo,dequeueTest){
	lab3::fifo test3;
	test3.enqueue("1");
	test3.enqueue("2");
	EXPECT_NO_THROW(test3.dequeue());
	EXPECT_NO_THROW(test3.dequeue());
	EXPECT_ANY_THROW(test3.dequeue());
	//Throws exception if trying to remove from empty queue
}

TEST(fifo,readTest){
	lab3::fifo test4;
	test4.enqueue("2");
	test4.enqueue("1");
	EXPECT_EQ("2",test4.top());
	test4.dequeue();
	EXPECT_EQ("1",test4.top());
	EXPECT_NO_THROW(test4.dequeue());
	EXPECT_ANY_THROW(test4.dequeue());

}

TEST(fifo,copyConstructor){
	lab3::fifo test5;
	test5.enqueue("2");
	test5.enqueue("1");
	lab3::fifo* test6 = new lab3::fifo(test5);
	EXPECT_EQ("2",test6->top());
	test6->dequeue();
	EXPECT_EQ("1",test6->top());
	delete test6;
}

TEST(fifo,stringConstructor){
	lab3::fifo test7("Test String");
	EXPECT_EQ("Test String",test7.top());
}

TEST(fifo,assignmentTest){
	lab3::fifo test8;
	lab3::fifo test9;

	test8.enqueue("Test1");
	test8.enqueue("Test2");
	test9.enqueue("Test1");
	test9.enqueue("Test2");

	EXPECT_EQ("Test1",test8.top());
	test8.dequeue();
	EXPECT_EQ("Test2",test8.top());

	test8 = test9;
	EXPECT_EQ("Test1",test8.top());
	test8.dequeue();
	EXPECT_EQ("Test2",test8.top());
}


//==========LIFO Tests==========//

TEST(lifo,constructorTest) {
	lab3::lifo test1;
	EXPECT_EQ(true,test1.is_empty());
	EXPECT_EQ(0,test1.size());
}

TEST(lifo,pushTest){
	lab3::lifo test2;
	EXPECT_NO_THROW(test2.push("1"));
    test2.push("String 2");
	EXPECT_EQ("String 2",test2.top());
}

TEST(lifo,popTest) {
	lab3::lifo test3;
    test3.push("Test 1");
    test3.push("Test 1");
	EXPECT_NO_THROW(test3.pop());
	EXPECT_EQ("Test 1",test3.top());
	EXPECT_NO_THROW(test3.pop());
	EXPECT_ANY_THROW(test3.pop());

}

TEST(lifo,readTest) {
	lab3::lifo test4;
    test4.push("String 1");
    test4.push("Test 2");
    test4.push("Popcorn");
	EXPECT_EQ("Popcorn",test4.top());
	test4.pop();
	EXPECT_EQ("Test 2",test4.top());
	EXPECT_NO_THROW(test4.pop());
	EXPECT_EQ("String 1",test4.top());
	EXPECT_NO_THROW(test4.pop());
	EXPECT_ANY_THROW(test4.pop());

}

TEST(lifo,copyConstructor) {
	lab3::lifo test5;
    test5.push("STR 2");
    test5.push("STR 2");
	EXPECT_NO_THROW(lab3::lifo testTest(test5));
	lab3::lifo* test6 = new lab3::lifo(test5);
	EXPECT_EQ("STR 2",test6->top());
	EXPECT_NO_THROW(delete test6);
}

TEST(lifo,assignmentTest) {
    lab3::lifo test7, test8;
    test7.push("Data 1");
    test7.push("Data 2");
    EXPECT_NO_THROW(test8 = test7);
    EXPECT_EQ("Data 2", test8.top());
    test8.pop();
    EXPECT_EQ("Data 2", test7.top());
    EXPECT_EQ("Data 1", test8.top());
}
