#include <sstream>
#include "gtest/gtest.h"
#include "calculator.h"


class Lab04Fixture : public ::testing::Test {

protected:
    virtual void SetUp() {
        testCalc = new lab4::calculator;
    }

    virtual void TearDown() {
        delete testCalc;
    }

public:
    lab4::calculator *testCalc;
};

TEST(calculator, addition) {
    lab4::calculator object1;
    std::stringstream test1("10+10");
    test1 >> object1;
    EXPECT_EQ(20,object1.calculate());
}
TEST(calculator,subtractionTest){
    lab4::calculator object2;
    std::stringstream test2("5-3-6");
   test2 >> object2;
    EXPECT_EQ(-4,object2.calculate());
}
TEST(calculator,divisionTest) {
    lab4::calculator object3;
    std::stringstream test3("100/20");
    test3 >> object3;
    EXPECT_EQ(5, object3.calculate());
}
TEST(calculator,multiplicationTest){
    lab4::calculator object4;
    std::stringstream test4("2*2*5");
    test4 >> object4;
    EXPECT_EQ(20,object4.calculate());
}
TEST(calculator,parenthesisTest){
    lab4::calculator object5;
    std::stringstream test5("5*(10-4)");
    test5>>object5;
    EXPECT_EQ(30,object5.calculate());
}
TEST(calculator,inputStream){
    std::stringstream stream_var;
    lab4::calculator obj8;
//    EXPECT_ANY_THROW(obj8.calculate());
    stream_var.str("((20+5)*4)/10");
    stream_var >> obj8;
    EXPECT_EQ(10,obj8.calculate());
}


