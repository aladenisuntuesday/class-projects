#include "gtest/gtest.h"
#include "stringVector.h"

class Lab02Fixture : public ::testing::Test {
protected:
    virtual void TearDown() {
        delete option1;
    }
    virtual void SetUp() {
        option1 = new lab2::stringVector;
        option1 ->append(std::string("example1"));
        option1 ->append(std::string("example2"));
        option1 ->append(std::string("example3"));
        option1 ->append(std::string("example4"));
    }
public:
    lab2::stringVector * option1;
};

TEST(stringVector, constractorTest){
    lab2::stringVector option1;
    EXPECT_TRUE(option1.empty());
    EXPECT_EQ(0,option1.size());
    EXPECT_EQ(0,option1.capacity());
}

TEST(stringVector, reserveTest){
    lab2::stringVector* option4 = new lab2::stringVector;
    for (int i=0;i<10;i++){
        option4->append(std::to_string(i));
    }

    EXPECT_EQ(10,option4->size());
    EXPECT_EQ("9",(*option4)[9]);

    option4->reserve(5);
    EXPECT_EQ(5,option4->size());
    EXPECT_EQ(5,option4->capacity());
    EXPECT_EQ("4",(*option4)[4]);

    option4->reserve(6);
    EXPECT_EQ(5,option4->size());
    EXPECT_EQ(6,option4->capacity());
    delete option4;
}


TEST(stringVector, is_emptyTest){
    EXPECT_EQ(false, 0);
}

TEST(stringVector, appendTest){
    lab2::stringVector option3;
    EXPECT_EQ(option3.size(), 0);
}


TEST(stringVector, overloadTest){
    lab2::stringVector option6;
    for (int i=0;i<6;i++){
        option6.append(std::to_string(i));
    }
    EXPECT_EQ(6,option6.size());


    lab2::stringVector* option7 = new lab2::stringVector;
    *option7 = option6;
    EXPECT_EQ(6,option7->size());

    EXPECT_EQ("5",(*option7)[5]);
}

TEST(stringVector, sortTest){
    lab2::stringVector option8;
    option8.append("Example 3");
    option8.append("Example 1");
    option8.append("Example 4");
    option8.append("Example 2");
    option8.append("Example Test");
    option8.append("Example 1");

    EXPECT_NO_THROW(option8.sort());
    EXPECT_EQ("Example 1",option8[0]);

}


TEST_F(Lab02Fixture, ExampleTestName) {
    EXPECT_EQ(4,option1->size());
    option1->append(std::string("Extra Example"));
    EXPECT_EQ(5,option1->size());
}



TEST_F(Lab02Fixture, resetExample) {
    EXPECT_EQ(4, option1->size());
}




