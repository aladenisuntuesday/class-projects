#include "lifo.h"

namespace lab3 {
    lifo::lifo() : index(0) {
        lifo_storage.set_length(100);
    }

    lifo::lifo(std::string input_string) {
        lifo_storage.set_length(100);
        lifo_storage[0] = input_string;
        index = 1;
    }

    lifo::lifo(const lifo &original) {
        this->lifo_storage = original.lifo_storage;
        this->index = original.index;
    }

    lifo::~lifo() {

    }

    lifo &lifo::operator=(const lifo &right) {
        this->lifo_storage = right.lifo_storage;
        this->index = right.index;
        return *this;
    }

    bool lifo::is_empty() {
        return (index == 0);
    }

    int lifo::size() {
        //if(!is_empty())
        return index;
    }

    std::string lifo::top() {
        return (lifo_storage[index]);

    }

    void lifo::push(std::string input) {
        if (100 == index) {
            throw "No storage available";
        } else {
            //
            lifo_storage[index + 1] = input;
            index++;
        }
    }

    void lifo::pop() {
        if(0 == index){
    throw "Empty storage";
}
        else{
            lifo_storage[index] = "";
            index--;
        }
    }
}