#include "fifo.h"

namespace lab3 {
    fifo::fifo() : front_index(0), back_index(0) {
        fifo_storage.set_length(100);
    }

    fifo::fifo(std::string input_string) {
        fifo_storage.set_length(100);
        fifo_storage[0] = input_string;
        front_index = 0;
        back_index = 1;
    }

    fifo::fifo(const fifo &original) {
         this->fifo_storage = original.fifo_storage;
        this->front_index = original.front_index;
        this->back_index = original.back_index;
    }

    fifo::~fifo() {
    }

    fifo &fifo::operator=(fifo &right) {
        for (int i = 0; i < right.back_index; i++) {
            this->fifo_storage[i] = right.fifo_storage[i];
        }
        this->front_index = right.front_index;
        this->back_index = right.back_index;
        return *this;
    }

    bool fifo::is_empty()  {
        return (front_index == back_index);
    }

    int fifo::size() {
        return (back_index);
    }

    std::string fifo::top() {
        return (fifo_storage[front_index]);
    }

    void fifo::enqueue(std::string input) {
        fifo_storage[back_index] = input;
        back_index = (back_index + 1)%100;
    }


    void fifo::dequeue() {
        if (is_empty()){
            throw "The storage is empty";
        }
        else {
            fifo_storage[back_index] = " ";
            front_index = (front_index+1)%100;
        }
    }
}
