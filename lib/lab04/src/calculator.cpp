#include "calculator.h"
#include <iostream>
#include <string>


namespace lab4 {

    //Check if its an operatior
    bool calculator::is_operator(const std::string &input_char) {
        return input_char == "+" || "-" == input_char || input_char == "*" || input_char == "/" || input_char == "(" ||
               input_char == ")";
        //Bryan posted on this function
    }

    // Check if its a number
    bool calculator::is_number(std::string input_char) {
        return (input_char) >= "0" && (input_char) <= "9";
    }

///convert: parse to infix
    void calculator::parse_to_infix(std::string& input_expression) {
        std::string::iterator position = input_expression.begin();
        std::string::iterator ending = input_expression.end();

        for (position = input_expression.begin();
             position != input_expression.end() && *position == ' '; position++) {
            is_number(std::string(position, position + 1));
        }
    }

    void calculator::convert_to_postfix(lab3::fifo infix_expression) {
        lab3::lifo op_stack;
        std::string current_token;
        while (!infix_expression.is_empty()) {
            current_token = infix_expression.top();
            infix_expression.dequeue();
            if (is_number(current_token)) {
                //push it
                postfix_expression.enqueue(current_token);

            } else if (is_operator(current_token)) {
                while (!op_stack.is_empty() && operator_priority(current_token) <= operator_priority(op_stack)) {
                    postfix_expression.enqueue(op_stack);
                    op_stack.pop();
                }
                op_stack.push(current_token);
            }
            if (current_token == "(") {
                op_stack.push(current_token);
            } else {}
            if (current_token == ")") {
                while (op_stack.top() != "(") {
                    postfix_expression.enqueue(op_stack.top());
                    op_stack.pop();
                }
                //  op_stack.pop();
            }
        }
        while (op_stack.is_empty()) {
            postfix_expression.enqueue(op_stack.top());
            op_stack.pop(); //Not too sure about this
        } 

    }

    //Default constructor
    calculator::calculator() {
    }

    //Constructor
    calculator::calculator(std::string &input_expression){
        parse_to_infix(input_expression);
        convert_to_postfix(infix_expression);
    }


    std::istream &operator>>(std::istream &stream, calculator &RHS) {
        std::string temp;
        std::string expression;

        while (stream >> temp) {
            expression.append(temp);
        }
        while (!RHS.infix_expression.is_empty()) {
            RHS.infix_expression.dequeue();
        }
        while (!RHS.postfix_expression.is_empty()) {
            RHS.postfix_expression.dequeue();
        }

        RHS.parse_to_infix(expression);
        RHS.convert_to_postfix(RHS.infix_expression);
        return stream;

    }

        int lab4::calculator::calculate() {
        lab3::lifo stack_calc;
        int calc1= 0, calc2 = 0;

        int j=0;
        while(j<postfix_expression.size()){
            std::string token;
            token = postfix_expression.top();

            if(is_number(token)){
                stack_calc.push(token);
            }

            else if (is_operator(token)){
                calc1 = stoi(stack_calc.top());
                stack_calc.pop();
                calc2 = stoi(stack_calc.top());
                stack_calc.pop();
                int result = get_operator(token, calc1, calc2);
                std::string convert = std::to_string(result);
                stack_calc.push(convert);
            }
        }
        return 0;
    }

    std::ostream &operator<<(std::ostream &stream, calculator &RHS) {
        return stream;
    }

    int calculator::operator_priority(std::string operator_in) {
        int precedence;
        if (operator_in == "+" ) {
            precedence = 2;
        }
        if (operator_in == "-") {
            precedence = 2;
        }
        if(operator_in == "*"){
            precedence = 3;
        }
        if (operator_in == "/") {
            precedence = 3;
        }
    return precedence;
    //Precedence checker
}
    int calculator::get_operator(std::string &operation, int calc1, int calc2 ){
        if(operation == "+") return calc1 +calc2;
        else if(operation == "-") return calc1 - calc2;
        else if(operation == "*") return calc1 * calc2;
        else if(operation == "/") return calc1 / calc2;

        else {
            return -1;
        };

    }


}
