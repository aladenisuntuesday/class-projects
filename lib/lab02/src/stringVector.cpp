#include "stringVector.h"
#include "expressionstream.h"
namespace lab2 {
    stringVector::stringVector() : length(0), allocated_length(0), data(nullptr) {
    }

    lab2::stringVector::~stringVector() {
        delete[] data;
    }

    unsigned stringVector::size() const {
        return length;
    }

    unsigned stringVector::capacity() const {
        return allocated_length;
    }


    void stringVector::set_length(unsigned new_length)
    {
        reserve(new_length);
        length = new_length;
    }

    void stringVector::reserve(unsigned new_size) {
        std::string *input = new std::string[new_size];
        if (new_size == allocated_length) {
            return;
        }
        for (int i = 0; i < length && i < new_size; i++) {
            input[i] = data[i];
        }
        if (length > new_size) {
            length = new_size;
        }
        allocated_length = new_size;
        delete[] data;
        data = input;
    }


    bool stringVector::empty() const {
        return (length == 0);
    }


    void stringVector::append(std::string new_data) { //FIX IT!
        if (length < allocated_length) {
            data[length] = new_data;
        } else {
            reserve(allocated_length == 0 ? 1 : allocated_length * 2);
            data[length] = new_data;
        }
        length++;
    }

    void stringVector::swap(unsigned pos1, unsigned pos2) {
        std::string temp;
        if ((pos1 >= length) || pos2 >= length) {
            return;
        } else {
            temp = data[pos1];
            data[pos1] = data[pos2];
            data[pos2] = temp;
        }
    }

    stringVector &stringVector::operator=(stringVector const &rhs) {
        //copying values from rhs to data
        if (this == &rhs) {
            return *this;
        }
        delete[] data;
        length = rhs.length;
        allocated_length = rhs.allocated_length;
        this->data = new std::string[allocated_length];
        for (int i = 0; i < length; i++) {
            this->data[i] = rhs.data[i];
        }
        return *this;
    }

    std::string &stringVector::operator[](unsigned position) {
        if (position < length) {
            return data[position];
        }
        else{
            throw "position is not less than length";
        }
    }

    void stringVector::sort() {
        std::string temp;
        for (int i = length; i > 0; i--) {
            for (int k = 0; k < i; k++) {
                if (data[k].compare(data[i + 1]) > 0) {
                    temp = data[k];
                    data[k] = data[k + 1];
                    data[k + 1] = temp;
                }
            }
        }
    }

}